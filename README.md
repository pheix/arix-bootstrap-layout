# Bootstrap v4 layout for ARIX.su userarea

## Annotation

This repository contains pages layout for Arix.su web-site.

## License information

You can redistribute this layout pack and/or modify it under the terms of the Artistic License 2.0.

## Author

Please contact via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).

## See more

[ARIX.su](https://arix.su)

[Pheix CMS](https://pheix.org)

[Bootstrap v4.0](https://getbootstrap.com/docs/4.0/getting-started/download/)

[The Artistic License 2.0](https://opensource.org/licenses/Artistic-2.0)
